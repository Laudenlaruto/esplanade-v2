# Esplanade

[![pipeline status](https://gitlab.com/Laudenlaruto/esplanade-v2/badges/develop/pipeline.svg)](https://gitlab.com/Laudenlaruto/esplanade-v2/-/commits/develop)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=Laudenlaruto_esplanade-v2&metric=alert_status)](https://sonarcloud.io/dashboard?id=Laudenlaruto_esplanade-v2)
[![Netlify Status](https://api.netlify.com/api/v1/badges/afe66848-2c78-496f-af61-26efbf41f3ca/deploy-status)](https://app.netlify.com/sites/esplanade/deploys)


## Documentation

- [Configuration](doc/../README.md)
- [Gatsby starter documentation](src/README.md)
