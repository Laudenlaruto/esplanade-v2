import React from 'react';
import Layout from '../components/Layout';
import Header from '../components/Header';
import Landing from '../sections/LandingPage';
import About from '../sections/About';
import Projects from '../sections/Projects';
import Writing from '../sections/Writing';
import Footer from '../components/Footer';
import Certification from '../sections/Certifications';

const IndexPage = () => (
  <Layout>
    <Header />
    <Landing />
    <About />
    <Certification />
    <Projects />
    <Writing />
    <Footer />
  </Layout>
);

export default IndexPage;
