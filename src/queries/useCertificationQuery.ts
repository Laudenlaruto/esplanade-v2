import { graphql, useStaticQuery } from 'gatsby';
import { Certification } from '../types';

export type QueryResponse = {
  contentfulAbout: {
    certifications: {
      id: string;
      name: string;
      cert: string;
      publishedDate: string;
      tag: string;
      logo: {
        title: string;
        image: {
          src: string;
        };
      };
    }[];
  };
};

export const useCertificationQuery = (): Certification[] => {
  const { contentfulAbout } = useStaticQuery<QueryResponse>(graphql`
    query CertificationQuery {
      contentfulAbout {
        certifications {
          id
          name
          cert: certUrl
          publishedDate(formatString: "YYYY")
          tag
          logo {
            title
            image: resize(width: 200, quality: 100) {
              src
            }
          }
        }
      }
    }
  `);

  return contentfulAbout.certifications.map(({ logo, ...rest }) => ({
    ...rest,
    logo: {
      alt: logo.title,
      src: logo.image.src,
    },
  }));
};
