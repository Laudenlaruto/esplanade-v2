import { graphql, useStaticQuery } from 'gatsby';
import { Post } from '../types';

export type QueryResponse = {
  contentfulAbout: {
    posts: {
      id: string;
      name: string;
      post: string;
      publishedDate: string;
      tag: string;
      logo: {
        title: string;
        image: {
          src: string;
        };
      };
    }[];
  };
};

export const usePostQuery = (): Post[] => {
  const { contentfulAbout } = useStaticQuery<QueryResponse>(graphql`
    query PostQuery {
      contentfulAbout {
        posts {
          id
          name
          post: postUrl
          publishedDate(formatString: "YYYY")
          tag
          logo {
            title
            image: resize(width: 200, quality: 100) {
              src
            }
          }
        }
      }
    }
  `);

  return contentfulAbout.posts.map(({ logo, ...rest }) => ({
    ...rest,
    logo: {
      alt: logo.title,
      src: logo.image.src,
    },
  }));
};
