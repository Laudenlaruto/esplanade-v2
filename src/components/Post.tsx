import React from 'react';
import SocialLink from '../components/SocialLink';
import ImageLabel from './ImageLabel';
import Hide from '../components/Hide';
import { Box, Flex, Image, Text } from 'rebass/styled-components';
import styled from 'styled-components';
import { Post as PostType } from '../types';
import { Card } from './Card';

type Props = PostType;

const Post = ({
  name,
  post,
  tag,
  publishedDate,
  logo,
}: Props) => (
  <Card p={0}>
    <Flex style={{ height: CARD_HEIGHT }}>
      <TextContainer>
        <span>
          <Title my={4} pb={2} color="text">
            {name}
          </Title>
        </span>
      </TextContainer>
      <ImageContainer>
        <PostImage {...logo} />
        <PostTag>
          <Flex
            m={1}
            style={{
              float: 'right',
            }}
          >
            <Box mx={1} fontSize={4}>
              <SocialLink name="link" icon="globe" url={post} />
            </Box>
          </Flex>
          <ImageLabel bg="primary" color="white" position="bottom-right" round>
            {tag}
          </ImageLabel>
          <Hide query="md">
            <ImageLabel bg="muted">{publishedDate}</ImageLabel>
          </Hide>
        </PostTag>
      </ImageContainer>
    </Flex>
  </Card>
);

const CARD_HEIGHT = '200px';

const MEDIA_QUERY_SMALL = '@media (max-width: 400px)';

const Title = styled(Text)`
  font-size: 12px;
  font-weight: 600;
  text-transform: uppercase;
  display: table;
  /* border-bottom: ${({ theme }) => theme.colors.primary} 5px solid; */
`;

const TextContainer = styled.div`
  display: flex;
  flex-direction: column;
  padding: 6px;
  width: 100%;
  width: calc(100% - ${CARD_HEIGHT});

  ${MEDIA_QUERY_SMALL} {
    width: calc(100% - (${CARD_HEIGHT} / 2));
  }
`;

const ImageContainer = styled.div`
  margin: auto;
  width: ${CARD_HEIGHT};

  ${MEDIA_QUERY_SMALL} {
    width: calc(${CARD_HEIGHT} / 2);
  }
`;

const PostImage = styled(Image)`
  width: ${CARD_HEIGHT};
  height: ${CARD_HEIGHT};
  padding: 40px;
  margin-top: 0px;

  ${MEDIA_QUERY_SMALL} {
    height: calc(${CARD_HEIGHT} / 2);
    width: calc(${CARD_HEIGHT} / 2);
    margin-top: calc(${CARD_HEIGHT} / 4);
    padding: 10px;
  }
`;

const PostTag = styled.div`
  position: relative;
  height: ${CARD_HEIGHT};
  top: calc(
    -${CARD_HEIGHT} - 3.5px
  ); /*don't know why I have to add 3.5px here ... */

  ${MEDIA_QUERY_SMALL} {
    top: calc(-${CARD_HEIGHT} - 3.5px + (${CARD_HEIGHT} / 4));
  }
`;

export default Post;
