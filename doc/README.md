# Configuration

## CI

CI is configured via gitlab ci and the .gitlab-ci.yml
Test are ran on the develop branch

- E2E tests
  - Cypress
  - Percy
- SAST (Sonarcloud)

On Every branch and PR

- Snyk (Scan every PR)

## CD

CD is configured with netlify.
A 'test' deployment is made on every merge request to the master branch
When you push to the maste branch it goes to production!
